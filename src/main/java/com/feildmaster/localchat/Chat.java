package com.feildmaster.localchat;

import com.massivecraft.factions.P;
import java.io.File;
import java.io.IOException;
import java.util.*;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public final class Chat extends JavaPlugin {
    private final ChatListener listener = new ChatListener(this);
    private Map<String, String> cache = new HashMap<String, String>();
    private int range = 1000;
    private P factions;

    public void onLoad() {
        File file = new File(this.getDataFolder(), "config.yml");
        if (!file.exists() || !configContainsDefaults()) {
            getConfig().options().copyDefaults(true);
            try {
                getConfig().save(file);
            } catch (IOException ex) {}
        }

        loadConfig();
    }

    public void onEnable() {
        Plugin p = getServer().getPluginManager().getPlugin("Factions");
        if (p instanceof P) {
            factions = (P) p;
        } else if (p != null) {
            getLogger().warning("Error when loading Factions... (unsupported version?)");
        }

        getServer().getPluginManager().registerEvents(listener, this);
    }

    private void loadConfig() {
        range = (int) Math.pow(getConfig().getInt("local.range"), 2);
    }

    public int getLocalRange() {
        return range;
    }

    public String getGlobalPrefix() {
        return getConfigString("global.prefix");
    }

    public String getGlobalFormat() {
        return getConfigString("global.format");
    }

    public String getGlobalPermissionMessage() {
        return getConfigString("global.no_permission");
    }

    public String getLocalNullMessage() {
        return getConfigString("local.null_message");
    }

    private String getConfigString(String name) {
        String value = cache.get(name);
        if (value != null) {
            return value;
        }

        value = getConfig().getString(name);
        cache.put(name, value);
        return value;
    }

    public P getFactionsPlugin() {
        return factions;
    }

    private boolean configContainsDefaults() {
        if (getConfig().getDefaults() == null) {
            return true;
        }
        return getConfig().getKeys(true).containsAll(getConfig().getDefaults().getKeys(true));
    }
}
