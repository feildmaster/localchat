package com.feildmaster.localchat;

import java.util.HashSet;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;

public class ChatListener implements Listener {
    private Chat plugin;

    public ChatListener(Chat p) {
        plugin = p;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerChat(PlayerChatEvent event) {
        // If they're faction chatting... don't mess with it
        if (plugin.getFactionsPlugin() != null && plugin.getFactionsPlugin().isPlayerFactionChatting(event.getPlayer())) {
            return;
        }

        if (event.getMessage().startsWith(plugin.getGlobalPrefix())) {
            if (!event.getPlayer().hasPermission("localchat.global")) {
                if (plugin.getGlobalPermissionMessage() != null && plugin.getGlobalPermissionMessage().length() > 0) {
                    event.getPlayer().sendMessage(plugin.getGlobalPermissionMessage());
                }
                event.setCancelled(true);
                return;
            }
            event.setMessage(event.getMessage().substring(plugin.getGlobalPrefix().length()));
            event.setFormat(plugin.getGlobalFormat());
            return;
        }

        for (Player r : new HashSet<Player>(event.getRecipients())) {
            if (outOfRange(event.getPlayer().getLocation(), r.getLocation())) {
                event.getRecipients().remove(r);
            }
        }

        if (event.getRecipients().size() == 1) {
            event.getPlayer().sendMessage(plugin.getLocalNullMessage());
            event.setCancelled(true);
        }
    }

    private Boolean outOfRange(Location l, Location ll) {
        if (l.equals(ll)) {
            return false;
        } else if (l.getWorld() != ll.getWorld()) {
            return true;
        }
        return l.distanceSquared(ll) > plugin.getLocalRange();
    }
}
